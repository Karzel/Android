package com.example.karzel.todolist;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.Time;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Time today = new Time(Time.getCurrentTimezone());
    DBAdapter myDb;
    EditText etTasks;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etTasks = (EditText) findViewById(R.id.editTextTask);
        openDB();
        populateListView();
        listViewItemClick();
        listViewItemLongClick();
    }

    private void openDB(){
        myDb = new DBAdapter(this);
        myDb.open();
    }

    public void onClick_AddTask(View w){
        today.setToNow();
        String timestamp = today.format("%Y-%m-%d %H:%M:%S");
        if(!TextUtils.isEmpty(etTasks.getText().toString())){
            myDb.insertRow(etTasks.getText().toString(), timestamp);
        }
        populateListView();
    }

    private void populateListView(){
        Cursor cursor = myDb.getAllRows();
        String[] fromFieldNames = new String[]{DBAdapter.KEY_ROWID,DBAdapter.KEY_TASK};
        int[] toViewIDs = new int[] {R.id.textViewItemNumber, R.id.textViewItemTask};
        SimpleCursorAdapter myCursorAdapter;
        myCursorAdapter = new SimpleCursorAdapter(getBaseContext(),R.layout.item_layout,cursor,fromFieldNames,toViewIDs,0);
        ListView myList = (ListView) findViewById(R.id.listViewTasks);
        myList.setAdapter(myCursorAdapter);
    }

    private void updateTask(long id) {
        Cursor cursor = myDb.getRow(id);
        if(cursor.moveToFirst()){
            String task = etTasks.getText().toString();
            today.setToNow();
            String date = today.format("%Y-%m-%d %H:%M:%S");
            myDb.updateRow(id,task,date);
        }
        cursor.close();
    }
    private void listViewItemClick(){
        ListView myList = (ListView) findViewById(R.id.listViewTasks);
        myList.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                updateTask(id);
                populateListView();
                displayToast(id);
            }
        });
    }

    public void onClick_DeleteTasks(View w){
        myDb.deleteAll();
        populateListView();
    }

    private void listViewItemLongClick(){
        ListView myList = (ListView) findViewById(R.id.listViewTasks);
        myList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                myDb.deleteRow(id);
                populateListView();
                return false;
            }
        });
    }

    private void displayToast(long id){
        Cursor cursor = myDb.getRow(id);
        if(cursor.moveToFirst()){
            long idDB = cursor.getLong(DBAdapter.COL_ROWID);
            String task = cursor.getString(DBAdapter.COL_TASK);
            String date = cursor.getString(DBAdapter.COL_DATE);

            String message = "ID: "+ idDB + "\n"+"Task: "+task+"\n"+"Date: "+date;

            Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
        }
        cursor.close();

    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        closeDB();
    }

    private void closeDB(){
        myDb.close();
    }
}
