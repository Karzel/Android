package com.example.karzel.biegamy.gps;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.example.karzel.biegamy.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.concurrent.TimeUnit;


public class GPSImplementation implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private Context context;
    private TextView tvDistanceOverall;
    private TextView tvTimeOverall;
    private TextView tvTempo;
    private TextView tvTempoOverall;
    private MediaPlayer mPlayer;
    private MediaPlayer mRunRest;
    private MediaPlayer mFastSlow;
    private boolean trainingStarted;
    private double startTime;
    private double endTime;
    private double startKilometerTime;
    private double endKilometerTime;
    private double overallDistance;
    private double halfminuteTempoTime;
    private double halfminuteDist;
    private long timeOverall;
    private double distance;
    private long time;
    private double IntervalTrainingTime;
    private double IntervalRestTime;
    private boolean IntervalNumbersOn;
    private int IntervalNumbers;
    private int IntervalCounter;
    private double TempoRequested;
    private boolean TempoFollower;
    private double nextRun;
    private int odometer;
    private boolean VoiceComands;
    private double nextRest;
    private boolean IntervalTraining;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location mLastLocation;
    private Location myCurrentLocation;
    private boolean mRequestingLocationUpdates;

    public GPSImplementation(Context c) {
        this.context=c;
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        timeOverall = time = 0;
        overallDistance = distance = 0;
        mRequestingLocationUpdates = true;
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(3000);
        mLocationRequest.setFastestInterval(1000);

        tvDistanceOverall = (TextView) ((Activity)context).findViewById(R.id.textViewDistance);
        tvTempo = (TextView) ((Activity)context).findViewById(R.id.textViewTempo);
        tvTempoOverall = (TextView) ((Activity)context).findViewById(R.id.textViewTempoOverall);
        tvTimeOverall = (TextView) ((Activity)context).findViewById(R.id.textViewTime);

        TempoRequested = 0;
        halfminuteTempoTime = 0;
        halfminuteDist = 0;
        odometer = 1;
        trainingStarted = false;
        TempoFollower = false;
        IntervalTraining = false;
        IntervalNumbersOn = false;
        VoiceComands = true;

        mPlayer = new MediaPlayer();
        mPlayer.setVolume(1.0f,1.0f);
        mRunRest = new MediaPlayer();
        mRunRest.setVolume(1.0f,1.0f);
        mFastSlow = new MediaPlayer();
        mFastSlow.setVolume(1.0f,1.0f);


    }

    //region Setters / Getters
    public Context getContext() {
        return context;
    }
    public boolean isTrainingStarted() {
        return trainingStarted;
    }
    public boolean isIntervalTraining() {
        return IntervalTraining;
    }
    public double getIntervalTrainingTime() {
        return IntervalTrainingTime;
    }

    public void setIntervalTrainingTime(double intervalTrainingTime) {
        IntervalTrainingTime = intervalTrainingTime;
        nextRun = timeOverall;
    }
    public boolean isIntervalNumbersOn() {
        return IntervalNumbersOn;
    }

    public boolean isVoiceComands() {
        return VoiceComands;
    }

    public void setVoiceComands(boolean voiceComands) {
        VoiceComands = voiceComands;
    }

    public void setIntervalNumbersOn(boolean intervalNumbersOn) {
        IntervalNumbersOn = intervalNumbersOn;
    }


    public int getIntervalNumbers() {
        return IntervalNumbers;
    }

    public void setIntervalNumbers(int intervalNumbers) {
        IntervalNumbers = intervalNumbers;
        IntervalCounter = 0;
    }
    public double getIntervalRestTime() {
        return IntervalRestTime;
    }

    public void setIntervalRestTime(double intervalRestTime) {
        IntervalRestTime = intervalRestTime;
        nextRest = timeOverall + IntervalTrainingTime;
    }
    public void setIntervalTraining(boolean intervalTraining) {
        IntervalTraining = intervalTraining;
    }

    public boolean isTempoFollower() {
        return TempoFollower;
    }

    public void setTempoFollower(boolean tempoFollower) {
        TempoFollower = tempoFollower;
    }


    public void setTrainingStarted(boolean trainingStarted) {
        this.trainingStarted = trainingStarted;
    }
    public double getTempoRequested() {
        return TempoRequested;
    }

    public void setTempoRequested(double tempoRequested) {
        TempoRequested = tempoRequested;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public double getStartTime() {
        return startTime;
    }

    public void setStartTime(double startTime) {
        this.startTime = startTime;
    }

    public double getEndTime() {
        return endTime;
    }

    public void setEndTime(double endTime) {
        this.endTime = endTime;
    }

    public double getOverallDistance() {
        return overallDistance;
    }

    public void setOverallDistance(double overallDistance) {
        this.overallDistance = overallDistance;
    }

    public long getTimeOverall() {
        return timeOverall;
    }

    public void setTimeOverall(long timeOverall) {
        this.timeOverall = timeOverall;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = (long)time;
    }

    public GoogleApiClient getmGoogleApiClient() {
        return mGoogleApiClient;
    }

    public void setmGoogleApiClient(GoogleApiClient mGoogleApiClient) {
        this.mGoogleApiClient = mGoogleApiClient;
    }

    public LocationRequest getmLocationRequest() {
        return mLocationRequest;
    }

    public void setmLocationRequest(LocationRequest mLocationRequest) {
        this.mLocationRequest = mLocationRequest;
    }

    public Location getmLastLocation() {
        return mLastLocation;
    }

    public void setmLastLocation(Location mLastLocation) {
        this.mLastLocation = mLastLocation;
    }

    public Location getMyCurrentLocation() {
        return myCurrentLocation;
    }

    public void setMyCurrentLocation(Location myCurrentLocation) {
        this.myCurrentLocation = myCurrentLocation;
    }

    public boolean ismRequestingLocationUpdates() {
        return mRequestingLocationUpdates;
    }

    public void setmRequestingLocationUpdates(boolean mRequestingLocationUpdates) {
        this.mRequestingLocationUpdates = mRequestingLocationUpdates;
    }
//endregion

    @Override
    public void onLocationChanged(Location location) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        myCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        endTime = SystemClock.elapsedRealtime();
        distance = getDistance(mLastLocation.getLatitude(), mLastLocation.getLongitude(), myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());
        distance = Math.round(distance*1000);
        distance /= 1000;
        time = (long)(endTime - startTime);
        startTime = endTime;
        mLastLocation = myCurrentLocation;
        if(trainingStarted){
        timeOverall += time;
        halfminuteTempoTime +=time;
        fillingData();
            if(VoiceComands && IntervalTraining){
                if(IntervalNumbersOn){
                    if(IntervalCounter<IntervalNumbers) intervalTraining();
                }
                else intervalTraining();
            }
        }
    }
    private void intervalTraining(){
        long timeO = TimeUnit.MILLISECONDS.toSeconds(timeOverall);
        if(timeO>=nextRest){
            nextRun=nextRest+IntervalRestTime;
            nextRest+=IntervalRestTime+IntervalTrainingTime;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    mRunRest = MediaPlayer.create(context, R.raw.rest);
                    mRunRest.start();
                    SystemClock.sleep(1000);
                    mRunRest.release();
                }
            }).start();

        }else if(timeO>=nextRun && nextRun!=0){
            IntervalCounter++;
            nextRun=nextRest+IntervalRestTime;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    mRunRest = MediaPlayer.create(context, R.raw.run);
                    mRunRest.start();
                    SystemClock.sleep(1000);
                    mRunRest.release();
                }
            }).start();
        }
    }
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if(ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.ACCESS_FINE_LOCATION) && ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.ACCESS_COARSE_LOCATION) );
            else{
                ActivityCompat.requestPermissions((Activity)context,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},MY_PERMISSIONS_REQUEST_LOCATION);
                ActivityCompat.requestPermissions((Activity)context,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }
    public void stopTraining(){
        distance = 0;
        overallDistance = 0;
        timeOverall = 0;
        TempoRequested = 0;
        odometer = 1;
        halfminuteTempoTime =0;
        halfminuteDist = 0;
        nextRun =0;
        nextRest =0;
        setTrainingStarted(false);
        clearViews();
        mPlayer.reset();
        mPlayer.release();
        mFastSlow.reset();
        mFastSlow.release();
        mRunRest.reset();
        mRunRest.release();
    }
    public void startTraining(){
        setTrainingStarted(true);
        startTime = SystemClock.elapsedRealtime();
        startKilometerTime = SystemClock.elapsedRealtime();
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }
    private double getDistance(double lat1, double lon1, double lat2, double lon2) {
        double latA = Math.toRadians(lat1);
        double lonA = Math.toRadians(lon1);
        double latB = Math.toRadians(lat2);
        double lonB = Math.toRadians(lon2);
        double cosAng = (Math.cos(latA) * Math.cos(latB) * Math.cos(lonB - lonA)) +
                (Math.sin(latA) * Math.sin(latB));
        double ang = Math.acos(cosAng);
        double dist = ang * 6371;
        return dist;
    }

    private void fillingData() {
        time = TimeUnit.MILLISECONDS.toSeconds(time);
        if (distance != 0 && time != 0) {
            overallDistance += distance;
            halfminuteDist += distance;
            if(halfminuteTempoTime >=30000) {
                halfminuteTempoTime = TimeUnit.MILLISECONDS.toSeconds((long)halfminuteTempoTime);
                long halfminuteTempo = (long)(halfminuteTempoTime/(halfminuteDist*1000)*1000);
                long tempoMin = TimeUnit.SECONDS.toMinutes(halfminuteTempo);
                if(TempoFollower && VoiceComands ) {
                    if (halfminuteTempo > TempoRequested + 15) wrongTempo('+');
                    else if (halfminuteTempo < TempoRequested - 15) wrongTempo('-');
                }
                tvTempo.setText("Tempo: " + String.format("%02d", tempoMin)+ ":"+String.format("%02d", halfminuteTempo-tempoMin*60));
                tvTempo.invalidate();
                halfminuteDist=0;
                halfminuteTempoTime=0;
            }
            long timeO = TimeUnit.MILLISECONDS.toSeconds(timeOverall);
            long speed = (long)(timeO/(overallDistance*1000) * 1000);
            long speedMin = TimeUnit.SECONDS.toMinutes(speed);
            if(VoiceComands && overallDistance>odometer) {
                odometer++;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        distanceCheck();
                    }
                }).start();}
            tvDistanceOverall.setText("Overall Distance: " + String.format("%.3f", overallDistance));
            tvDistanceOverall.invalidate();
            if(!(speedMin>30)) {
                tvTempoOverall.setText("Overall Tempo: " + String.format("%02d",speedMin) + ":" + String.format("%02d",speed - speedMin * 60));
                tvTempoOverall.invalidate();
            }
        }
        int hours = (int)TimeUnit.MILLISECONDS.toHours(timeOverall);
        int minutes = (int)TimeUnit.MILLISECONDS.toMinutes(timeOverall)-60*hours;
        int seconds = (int)TimeUnit.MILLISECONDS.toSeconds(timeOverall)-3600*hours-60*minutes;
        tvTimeOverall.setText("Overall Time:  " + hours + ":"+ String.format("%02d", minutes) + ":" + String.format("%02d", seconds));
        tvTimeOverall.invalidate();
    }

    private void distanceCheck() {
            endKilometerTime = SystemClock.elapsedRealtime();
            long kilometerTime = (long)(endKilometerTime - startKilometerTime);
            mPlayer = MediaPlayer.create(context, R.raw.kilometerin);
            mPlayer.start();
            SystemClock.sleep(1500);
            mPlayer.release();
            int hours = (int)TimeUnit.MILLISECONDS.toHours(kilometerTime);
            int minutes = (int)TimeUnit.MILLISECONDS.toMinutes(kilometerTime)-60*hours;
            int seconds = (int)TimeUnit.MILLISECONDS.toSeconds(kilometerTime)-3600*hours-60*minutes;
            if(hours!=0) {
                numbersTeller(hours, "hour");
            }
            numbersTeller(minutes, "minute");
            numbersTeller(seconds, "second");

            mPlayer = MediaPlayer.create(context, R.raw.overalltime);
            mPlayer.start();
            SystemClock.sleep(1500);
            mPlayer.release();
            hours = (int)TimeUnit.MILLISECONDS.toHours(timeOverall);
            minutes = (int)TimeUnit.MILLISECONDS.toMinutes(timeOverall)-60*hours;
            seconds = (int)TimeUnit.MILLISECONDS.toSeconds(timeOverall)-3600*hours-60*minutes;

            if(hours!=0) {
                numbersTeller(hours, "hour");
            }
            numbersTeller(minutes, "minute");
            numbersTeller(seconds, "second");
            startKilometerTime = endKilometerTime;


    }

    public void clearViews(){
        tvTempo.setText("Tempo: 0:00");
        tvDistanceOverall.setText("Overall Distance: 0,000");
        tvTempoOverall.setText("Overall Tempo: 0:00");
        tvTimeOverall.setText("Overall Time: 0:00:00");
    }

    private void numbersTeller(int value, String type) {
        int first = 0;
        int second;
        if(value>9){
            second = value%10;
            first = value/10;
        }
        else{
            second = value;
        }
        if(first!=0) numbersTeller2(first);
        numbersTeller2(second);

        switch(type){
            case "hour":
                mPlayer = MediaPlayer.create(context, R.raw.hours);
                break;
            case "minute":
                mPlayer = MediaPlayer.create(context, R.raw.minutes);
                break;
            case "second":
                mPlayer = MediaPlayer.create(context, R.raw.seconds);
                break;
            default:SystemClock.sleep(1000); break;
        }
        mPlayer.start();
        SystemClock.sleep(1000);
        mPlayer.release();
    }

    private void numbersTeller2(int value) {
        switch(value){
            case 0:
                mPlayer = MediaPlayer.create(context, R.raw.zero);
                break;
            case 1:
                mPlayer = MediaPlayer.create(context, R.raw.one);
                break;
            case 2:
                mPlayer = MediaPlayer.create(context, R.raw.two);
                break;
            case 3:
                mPlayer = MediaPlayer.create(context, R.raw.three);
                break;
            case 4:
                mPlayer = MediaPlayer.create(context, R.raw.four);
                break;
            case 5:
                mPlayer = MediaPlayer.create(context, R.raw.five);
                break;
            case 6:
                mPlayer = MediaPlayer.create(context, R.raw.six);
                break;
            case 7:
                mPlayer = MediaPlayer.create(context, R.raw.seven);
                break;
            case 8:
                mPlayer = MediaPlayer.create(context, R.raw.eight);
                break;
            case 9:
                mPlayer = MediaPlayer.create(context, R.raw.nine);
                break;
            default:SystemClock.sleep(1000); break;
        }
        mPlayer.start();
        SystemClock.sleep(1000);
        mPlayer.release();
    }

    private void wrongTempo(final char sign){
        new Thread(new Runnable() {
            @Override
            public void run() {
                if(sign=='-'){
                    mFastSlow = MediaPlayer.create(context, R.raw.tofast);
                }
                else{
                    mFastSlow = MediaPlayer.create(context, R.raw.toslow);
                }
                mFastSlow.start();
                SystemClock.sleep(1000);
                mFastSlow.release();
            }
        }).start();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }



}