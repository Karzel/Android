package com.example.karzel.biegamy;

import android.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.IntDef;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.Time;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.SimpleCursorAdapter;
import android.widget.Switch;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.example.karzel.biegamy.gps.GPSImplementation;

import java.util.concurrent.TimeUnit;


public class MapsActivity extends AppCompatActivity {

    private NumberPicker npTempoMin;
    private NumberPicker npTempoSec;
    private NumberPicker npIntTraMin;
    private NumberPicker npIntTraSec;
    private NumberPicker npIntRestMin;
    private NumberPicker npIntRestSec;
    private NumberPicker npIntNumber;
    private Button bDeleteHistory;
    private Button bStartStop;
    private Switch sTempo;
    private Switch sVoice;
    private Switch sInterval;
    private Switch sNumbers;
    private MediaPlayer mgoPlayer;
    private boolean voiceCommands;
    private GPSImplementation gps;
    Time today = new Time(Time.getCurrentTimezone());
    DBAdapter myDb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        gps = new GPSImplementation(this);

        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec spec1 = tabHost.newTabSpec("tab1");
        spec1.setContent(R.id.tabProgram);
        spec1.setIndicator(" Program");
        tabHost.addTab(spec1);

        TabHost.TabSpec spec2 = tabHost.newTabSpec("tab2");
        spec2.setContent(R.id.tabHistory);
        spec2.setIndicator("History");
        tabHost.addTab(spec2);

        TabHost.TabSpec spec3 = tabHost.newTabSpec("tab3");
        spec3.setContent(R.id.tabSettings);
        spec3.setIndicator("Settings");
        tabHost.addTab(spec3);

        npTempoMin = (NumberPicker) findViewById(R.id.npTempoMin);
        npTempoMin.setMaxValue(20);npTempoMin.setMinValue(0);npTempoMin.setWrapSelectorWheel(true); npTempoMin.setEnabled(false);
        npTempoSec = (NumberPicker) findViewById(R.id.npTempoSec);
        npTempoSec.setMaxValue(59);npTempoSec.setMinValue(0);npTempoSec.setWrapSelectorWheel(true); npTempoSec.setEnabled(false);
        npIntTraMin = (NumberPicker) findViewById(R.id.npInterTraMin);
        npIntTraMin.setMaxValue(20);npIntTraMin.setMinValue(0);npIntTraMin.setWrapSelectorWheel(true); npIntTraMin.setEnabled(false);
        npIntTraSec = (NumberPicker) findViewById(R.id.npInterTraSec);
        npIntTraSec.setMaxValue(59);npIntTraSec.setMinValue(0);npIntTraSec.setWrapSelectorWheel(true); npIntTraSec.setEnabled(false);
        npIntRestMin = (NumberPicker) findViewById(R.id.npInterRestMin);
        npIntRestMin.setMaxValue(20);npIntRestMin.setMinValue(0);npIntRestMin.setWrapSelectorWheel(true); npIntRestMin.setEnabled(false);
        npIntRestSec = (NumberPicker) findViewById(R.id.npInterRestSec);
        npIntRestSec.setMaxValue(59);npIntRestSec.setMinValue(0);npIntRestSec.setWrapSelectorWheel(true); npIntRestSec.setEnabled(false);
        npIntNumber = (NumberPicker) findViewById(R.id.npInterNumber);
        npIntNumber.setMaxValue(100);npIntNumber.setMinValue(0);npIntNumber.setWrapSelectorWheel(true);npIntNumber.setEnabled(false);

        sVoice = (Switch) findViewById(R.id.switchVoices);
        sTempo = (Switch) findViewById(R.id.switchTempo);
        sInterval = (Switch) findViewById(R.id.switchInterval);
        sNumbers = (Switch) findViewById(R.id.switchNumberIntervals);
        sNumbers.setClickable(false);

        bStartStop = (Button) findViewById(R.id.buttonStartStop);
        bDeleteHistory = (Button) findViewById(R.id.bDeleteHistory);
        gps.clearViews();
        mgoPlayer = new MediaPlayer();

        voiceCommands = true;

        openDB();
        populateListView();
        listViewItemLongClick();

//region Switch Listeners
        sNumbers.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked){
                    npIntNumber.setEnabled(false);
                    gps.setIntervalNumbersOn(false);
                }else{
                    npIntNumber.setEnabled(true);
                    gps.setIntervalNumbersOn(true);
                }
            }
        });

        sVoice.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked){
                    voiceCommands = false;
                    gps.setVoiceComands(false);
                    sTempo.setChecked(false);
                    sInterval.setChecked(false);
                    sNumbers.setChecked(false);
                    sTempo.setClickable(false);
                    sInterval.setClickable(false);
                    sNumbers.setClickable(false);
                }else{
                    voiceCommands = true;
                    gps.setVoiceComands(true);
                    sTempo.setClickable(true);
                    sInterval.setClickable(true);
                }
            }
        });

        sTempo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked){
                    npTempoMin.setEnabled(false);
                    npTempoSec.setEnabled(false);
                    gps.setTempoFollower(false);
                }else{
                    npTempoMin.setEnabled(true);
                    npTempoSec.setEnabled(true);
                    gps.setTempoFollower(true);
                }

            }
        });

        sInterval.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked){
                    npIntTraMin.setEnabled(false);
                    npIntTraSec.setEnabled(false);
                    npIntRestMin.setEnabled(false);
                    npIntRestSec.setEnabled(false);
                    gps.setIntervalTraining(false);
                    sNumbers.setChecked(false);
                    sNumbers.setClickable(false);
                }else{
                    npIntTraMin.setEnabled(true);
                    npIntTraSec.setEnabled(true);
                    npIntRestMin.setEnabled(true);
                    npIntRestSec.setEnabled(true);
                    gps.setIntervalTraining(true);
                    sNumbers.setClickable(true);
                }

            }
        });
//endregion
    }

    private void openDB(){
        myDb = new DBAdapter(this);
        myDb.open();
    }

    private void populateListView(){
        Cursor cursor = myDb.getAllRows();
        String[] fromFieldNames = new String[]{DBAdapter.KEY_ROWID,DBAdapter.KEY_LENGTH, DBAdapter.KEY_TIME, DBAdapter.KEY_TEMPO, DBAdapter.KEY_DATE};
        int[] toViewIDs = new int[] {R.id.textViewItemNumber, R.id.textViewItemLength, R.id.textViewItemTime, R.id.textViewItemTempo, R.id.textViewItemDate};
        SimpleCursorAdapter myCursorAdapter;
        myCursorAdapter = new SimpleCursorAdapter(getBaseContext(),R.layout.item_layout,cursor,fromFieldNames,toViewIDs,0);
        ListView myList = (ListView) findViewById(R.id.listViewTasks);
        myList.setAdapter(myCursorAdapter);
    }
    public void onStopTrainingClick(){
        today.setToNow();
        String timestamp = today.format("%d-%m");
        int timeHour = (int)TimeUnit.MILLISECONDS.toHours(gps.getTimeOverall());
        int timeMin = (int) TimeUnit.MILLISECONDS.toMinutes(gps.getTimeOverall()-60*timeHour);
        int timeSeconds = (int)TimeUnit.MILLISECONDS.toSeconds(gps.getTimeOverall()-3600*timeHour-60*timeMin);
        long timeO = TimeUnit.MILLISECONDS.toSeconds(gps.getTimeOverall());
        long speed = (long)(timeO/(gps.getOverallDistance()*1000) * 1000);
        long speedMin = TimeUnit.SECONDS.toMinutes(speed);
        if(speedMin>20){
            if(timeHour!=0) myDb.insertRow("Dist: "+ String.format("%.3f",gps.getOverallDistance()), "Time: "+ timeHour + ":"+String.format("%02d",timeMin) + ":" + String.format("%02d",timeSeconds),"Tempo: WRONG"  ,"Date: "+ timestamp);
            else myDb.insertRow("Dist: "+ String.format("%.3f",gps.getOverallDistance()), "Time: "+ String.format("%02d",timeMin) + ":" + String.format("%02d",timeSeconds), "Tempo: WRONG" ,"Date: "+timestamp);
        }
        else{
            if(timeHour!=0) myDb.insertRow("Dist: "+ String.format("%.3f",gps.getOverallDistance()), "Time: "+ timeHour + ":"+String.format("%02d",timeMin) + ":" + String.format("%02d",timeSeconds),"Tempo: " + String.valueOf(speedMin) + ":" + String.format("%02d",speed-speedMin*60) ,"Date: "+timestamp);
            else myDb.insertRow("Dist: "+ String.format("%.3f",gps.getOverallDistance()), "Time: "+ String.format("%02d",timeMin) + ":" + String.format("%02d",timeSeconds), "Tempo: " + String.valueOf(speedMin) + ":" + String.format("%02d",speed-speedMin*60) ,"Date: "+timestamp);

        }
         populateListView();
    }
    public void deleteHistoryButton(View w){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirm");
        builder.setMessage("Do you really want to remove whole training History?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                myDb.deleteAll();
                populateListView();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();

    }
    private void listViewItemLongClick(){

        ListView myList = (ListView) findViewById(R.id.listViewTasks);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        myList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, final long id) {

                builder.setTitle("Confirm");
                builder.setMessage("Do you want to delete this training?");

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        myDb.deleteRow(id);
                        populateListView();
                        dialog.dismiss();
                    }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();

               return false;
            }
        });
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        closeDB();
    }

    private void closeDB(){
        myDb.close();
    }


    public void onButtonClick(View w){
        if(gps.isTrainingStarted()) {
            bStartStop.setText("START");
            onStopTrainingClick();
            gps.stopTraining();
            displayToast();
        }
        else{
            gps.startTraining();
            bStartStop.setText("STOP");
                if(voiceCommands){
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        settingNumbers();
                        mgoPlayer = MediaPlayer.create(getApplicationContext(), R.raw.go);
                        mgoPlayer.setVolume(1.0f,1.0f);
                        mgoPlayer.start();
                        SystemClock.sleep(2000);
                        mgoPlayer.release();
                    }
                }).start();
                }
        }
    }

    private void settingNumbers() {
        gps.setTempoRequested(npTempoMin.getValue()*60+npTempoSec.getValue());
        gps.setIntervalNumbers(npIntNumber.getValue());
        gps.setIntervalTrainingTime(npIntTraMin.getValue()*60+npIntTraSec.getValue());
        gps.setIntervalRestTime(npIntRestMin.getValue()*60+npIntRestSec.getValue());
    }

    private void displayToast(){
            String message = "New training has been added to history!";
            Toast.makeText(MapsActivity.this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_menu, menu);
        return true;
    }

    public void onExitClose(MenuItem item) {
        System.exit(0);
    }

    @Override
    public void onStart(){
        gps.getmGoogleApiClient().connect();
        super.onStart();

    }


}