package com.example.karzel.remoteforrobot;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;


public class MainScreenActivity extends AppCompatActivity {

    Button PCBt;
    Button RaspBt;

    private static final String TAG = "OPENCVKA";

    static {
        if (!OpenCVLoader.initDebug()) {
            Log.i(TAG, "OpenCV loaded successfully");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

        PCBt = (Button) findViewById(R.id.buttonPC);
        RaspBt = (Button) findViewById(R.id.buttonRasp);

    }
    public void onPCBtClick(View v){
        Intent i = new Intent(getApplicationContext(), PCConnectionActivity.class);
        startActivity(i);
    }
    public void onRaspBtClick(View v){
        Intent i = new Intent(getApplicationContext(), RaspConnectionActivity.class);
        startActivity(i);
    }

}
