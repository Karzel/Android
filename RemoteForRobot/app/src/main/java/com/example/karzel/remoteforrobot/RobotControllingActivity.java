package com.example.karzel.remoteforrobot;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.service.carrier.CarrierMessagingService;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.VideoView;

import android.graphics.Bitmap;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.github.pwittchen.reactivesensors.library.ReactiveSensorEvent;
import com.github.pwittchen.reactivesensors.library.ReactiveSensorFilter;
import com.github.pwittchen.reactivesensors.library.ReactiveSensors;
import com.longdo.mjpegviewer.MjpegView;

import android.util.Log;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.WindowManager;

import android.os.Bundle;
import org.opencv.android.JavaCameraView;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.io.ByteArrayInputStream;
import java.io.DataInput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

import static java.lang.Math.round;

public class RobotControllingActivity extends AppCompatActivity implements CvCameraViewListener2{

    ToggleButton StartBt;
    private String IP;
    private String Port;
    private MjpegView view1;
    int turnLeft;

    int turnRight;
    boolean send;
    Button ToMenuBt;
    SeekBar SpeedBar;
    TextView SpeedValue;
    Socket socket = null;
    DataOutputStream DOS;


    private static final String TAG = "OCVSample::Activity";
    private CameraBridgeViewBase mOpenCvCameraView;
    Mat mRgba;
    Mat mRgbaF;
    Mat mRgbaT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_robot_controlling);


        turnLeft = 0;
        turnRight = 0;
        send = false;
        this.IP = getIntent().getExtras().getString("IP");
        this.Port = getIntent().getExtras().getString("Port");

        StartBt = (ToggleButton) findViewById(R.id.buttonStart);
        ToMenuBt = (Button) findViewById(R.id.buttonToMenuRasp);
        SpeedBar = (SeekBar) findViewById(R.id.speedBar);
        SpeedValue = (TextView) findViewById(R.id.textViewSpeedValue);
        SpeedBar.setEnabled(false);
        StartBt.setClickable(false);


        mOpenCvCameraView = (JavaCameraView) findViewById(R.id.show_camera_activity_java_surface_view);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);

        Connection();

        new ReactiveSensors(getApplicationContext()).observeSensor(Sensor.TYPE_ACCELEROMETER)
                .subscribeOn(Schedulers.computation())
                .filter(ReactiveSensorFilter.filterSensorChanged())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(reactiveSensorEvent -> {
                    SensorEvent event = reactiveSensorEvent.getSensorEvent();

                    float y = event.values[1];
                    if(y>=2.5) {turnLeft = 1; turnRight = 0;}
                    else if(y<=-2.5) {turnLeft = 0; turnRight = 1;}
                    else {turnLeft = 0; turnRight = 0;}
                });


        SpeedBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                SpeedValue.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        StartBt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    send = true;
                    ToMenuBt.setClickable(false);
                    SpeedBar.setEnabled(true);
                    sendingData();
                }
                else {
                    send=false;
                    ToMenuBt.setClickable(true);
                    SpeedBar.setProgress(0);
                    SpeedBar.setEnabled(false);
                    sendEnd();
                }

            }
        });

    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    Log.i("BLADBLAD", String.valueOf(status));
                    super.onManagerConnected(status);
                } break;
            }
        }
    };
    public RobotControllingActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }


    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {

        mRgba = new Mat(height, width, CvType.CV_8UC4);
        mRgbaF = new Mat(height, width, CvType.CV_8UC4);
        mRgbaT = new Mat(width, width, CvType.CV_8UC4);
    }

    public void onCameraViewStopped() {
        mRgba.release();
    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {

        // TODO Auto-generated method stub
        mRgba = inputFrame.rgba();
        // Rotate mRgba 90 degrees
        Core.transpose(mRgba, mRgbaT);
        Imgproc.resize(mRgbaT, mRgbaF, mRgbaF.size(), 0,0, 0);
        Core.flip(mRgbaF, mRgba, 1 );

        return mRgba; // This function must return
    }
    private void sendEnd() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    DOS.writeUTF("camera_off");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void sendingData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(send){
                try {
                    Double x = SpeedBar.getProgress()*0.2;
                    x= Double.valueOf(Math.round(x*100));
                    x/=100.0;
                    String speed = String.format("%2s", Double.toString(x).replace(' ','0'));
                    DOS.writeUTF("A,"+Integer.toString(turnLeft)+","+speed+","+Integer.toString(turnRight)+","+speed);
                    Thread.sleep(500);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                }
            }
        }).start();
    }

    private void Connection() {
        new Thread(new Runnable(){
            @Override
            public void run() {

                try {
                    socket = new Socket(IP,Integer.parseInt(Port));
                    DOS = new DataOutputStream(socket.getOutputStream());
                    DOS.writeUTF("camera_on");
                    while(!socket.isConnected()){}
                    turnOnButton();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    private void turnOnButton(){
        StartBt.setClickable(true);
    }

    public void onToMenuBtClick(View v){
        view1.stopStream();
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Intent i = new Intent(getApplicationContext(), MainScreenActivity.class);
        startActivity(i);
    }

}
