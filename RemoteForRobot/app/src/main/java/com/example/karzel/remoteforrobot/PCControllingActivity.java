package com.example.karzel.remoteforrobot;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;

public class PCControllingActivity extends AppCompatActivity {

    Button PCStartBt;
    Button BackBt;
    TextView ConnectionTV;
    private String IP;
    private String Port;


    Socket socket = null;
    DataOutputStream DOS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pccontrolling);

        this.IP = getIntent().getExtras().getString("IP");
        this.Port = getIntent().getExtras().getString("Port");

        ConnectionTV = (TextView) findViewById(R.id.textViewConnection);
        PCStartBt = (Button) findViewById(R.id.buttonPCStart);
        BackBt = (Button) findViewById(R.id.buttonToMenuPC);
        ConnectionTV.setText("Connecting!");
        PCStartBt.setClickable(false);
        Connection();
    }

    private void Connection() {
        new Thread(new Runnable(){
            @Override
            public void run() {

                try {
                    socket = new Socket(IP,Integer.parseInt(Port));
                    DOS = new DataOutputStream(socket.getOutputStream());
                    DOS.writeUTF("Connected");
                    while(!socket.isConnected()){}
                    turnOnButton();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    private void turnOnButton(){
        PCStartBt.setClickable(true);
        ConnectionTV.setVisibility(View.GONE);
    }

    public void onStartPCBtClick(View v) throws IOException {
        new Thread(new Runnable(){
            @Override
            public void run() {

                try {
                    DOS.writeUTF("Start Program");
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();

        PCStartBt.setClickable(false);
        PCStartBt.setText("");
        ConnectionTV.setVisibility(View.VISIBLE);
        ConnectionTV.setText("Program has been started!");
    }


    public void onToMenuBtClickPC(View v){
        Intent i = new Intent(getApplicationContext(), MainScreenActivity.class);
        startActivity(i);

    }
}
