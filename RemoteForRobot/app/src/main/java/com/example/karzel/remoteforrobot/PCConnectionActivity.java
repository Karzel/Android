package com.example.karzel.remoteforrobot;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class PCConnectionActivity extends AppCompatActivity {

    EditText IP;
    EditText Port;
    Button BackBt;
    Button PCConnBt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pcconnection);


        IP = (EditText) findViewById(R.id.editTextIP);
        Port = (EditText) findViewById(R.id.editTextPort);
        BackBt = (Button) findViewById(R.id.buttonBack);
        PCConnBt = (Button) findViewById(R.id.buttonPCConn);
    }

    public void onBackBtClick(View v){
        finish();
    }

    public void onPCConnBtClick(View v){
        Intent i = new Intent(getApplicationContext(), PCControllingActivity.class);
        i.putExtra("IP", IP.getText().toString());
        i.putExtra("Port", Port.getText().toString());
        startActivity(i);

    }
}
