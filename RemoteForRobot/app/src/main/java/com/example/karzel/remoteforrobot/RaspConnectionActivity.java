package com.example.karzel.remoteforrobot;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class RaspConnectionActivity extends AppCompatActivity {

    EditText Port;
    EditText IP;
    Button BackBt;
    Button RaspConnBt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection_rasp_screen);

        Port = (EditText) findViewById(R.id.editTextPort);
        IP = (EditText) findViewById(R.id.editTextIP);
        BackBt = (Button) findViewById(R.id.buttonBack);
        RaspConnBt = (Button) findViewById(R.id.buttonRaspConn);
    }

    public void onBackBtClick(View v){
        finish();
    }

    public void onRaspConnBtClick(View v){
        Intent i = new Intent(getApplicationContext(), RobotControllingActivity.class);
        i.putExtra("IP", IP.getText().toString());
        i.putExtra("Port", Port.getText().toString());
        startActivity(i);

    }

    /*
        view1 = (MjpegView) findViewById(R.id.mjpegview1);
        view1.setMode(MjpegView.MODE_FIT_WIDTH);
        view1.setAdjustHeight(true);
        view1.setAdjustWidth(true);
        view1.setUrl("http://192.168.1.155:8080/?action=stream");
        view1.setRecycleBitmap(true);
        view1.startStream();
*/
}
