package com.example.karzel.guitar;

import android.content.Intent;
import android.media.Image;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TabHost;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    ToggleButton tbtnE6;
    ToggleButton tbtnA;
    ToggleButton tbtnD;
    ToggleButton tbtnB;
    ToggleButton tbtnG;
    ToggleButton tbtnE1;

    ImageButton ibtnLoop;
    ImageButton ibtnStop;
    MediaPlayer mpGuitar;
    boolean loopIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec spec1 = tabHost.newTabSpec("tab1");
        spec1.setContent(R.id.tab1);
        spec1.setIndicator("Accoustic");
        tabHost.addTab(spec1);

        TabHost.TabSpec spec2 = tabHost.newTabSpec("tab2");
        spec2.setContent(R.id.tab2);
        spec2.setIndicator("Electric");
        tabHost.addTab(spec2);

        TabHost.TabSpec spec3 = tabHost.newTabSpec("tab3");
        spec3.setContent(R.id.tab3);
        spec3.setIndicator("Bass");
        tabHost.addTab(spec3);

        tbtnE6 = (ToggleButton) findViewById(R.id.toggleButton);
        tbtnA = (ToggleButton) findViewById(R.id.toggleButton2);
        tbtnD = (ToggleButton) findViewById(R.id.toggleButton3);
        tbtnG = (ToggleButton) findViewById(R.id.toggleButton4);
        tbtnB = (ToggleButton) findViewById(R.id.toggleButton5);
        tbtnE1 = (ToggleButton) findViewById(R.id.toggleButton6);

        ibtnLoop = (ImageButton) findViewById(R.id.imageButtonLoop);
        mpGuitar = new MediaPlayer();
        mpGuitar = MediaPlayer.create(this, R.raw.stringe6);
        loopIndicator = false;
        ibtnStop = (ImageButton) findViewById(R.id.imageButtonStop);

        ibtnLoop.setVisibility(View.VISIBLE);
        ibtnStop.setVisibility(View.GONE);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater findMenuItems = getMenuInflater();
        findMenuItems.inflate(R.menu.menu_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.action_exit:
                super.finish();
                return true;
            case R.id.action_Menu:
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    public void onToggleE6Click(View w){
        mpGuitar.stop();
        mpGuitar = MediaPlayer.create(this, R.raw.stringe6);
        mpGuitar.setLooping(loopIndicator);
        mpGuitar.start();

        tbtnA.setChecked(false);
        tbtnD.setChecked(false);
        tbtnB.setChecked(false);
        tbtnG.setChecked(false);
        tbtnE1.setChecked(false);

    }
    public void onToggleAClick(View w){
        mpGuitar.stop();
        mpGuitar = MediaPlayer.create(this, R.raw.stringa);
        mpGuitar.setLooping(loopIndicator);
        mpGuitar.start();
        tbtnE6.setChecked(false);
        tbtnD.setChecked(false);
        tbtnB.setChecked(false);
        tbtnG.setChecked(false);
        tbtnE1.setChecked(false);
    }
    public void onToggleDClick(View w){
        mpGuitar.stop();
        mpGuitar = MediaPlayer.create(this, R.raw.stringd);
        mpGuitar.setLooping(loopIndicator);
        mpGuitar.start();
        tbtnA.setChecked(false);
        tbtnE6.setChecked(false);
        tbtnB.setChecked(false);
        tbtnG.setChecked(false);
        tbtnE1.setChecked(false);
    }
    public void onToggleGClick(View w){
        mpGuitar.stop();
        mpGuitar = MediaPlayer.create(this, R.raw.stringg);
        mpGuitar.setLooping(loopIndicator);
        mpGuitar.start();
        tbtnA.setChecked(false);
        tbtnD.setChecked(false);
        tbtnB.setChecked(false);
        tbtnE6.setChecked(false);
        tbtnE1.setChecked(false);
    }
    public void onToggleBClick(View w){
        mpGuitar.stop();
        mpGuitar = MediaPlayer.create(this, R.raw.stringb);
        mpGuitar.setLooping(loopIndicator);
        mpGuitar.start();
        tbtnA.setChecked(false);
        tbtnD.setChecked(false);
        tbtnE6.setChecked(false);
        tbtnG.setChecked(false);
        tbtnE1.setChecked(false);
    }
    public void onToggleE1Click(View w){
        mpGuitar.stop();
        mpGuitar = MediaPlayer.create(this, R.raw.stringe1);
        mpGuitar.setLooping(loopIndicator);
        mpGuitar.start();
        tbtnA.setChecked(false);
        tbtnD.setChecked(false);
        tbtnB.setChecked(false);
        tbtnG.setChecked(false);
        tbtnE6.setChecked(false);
    }
    public void onButtonLoopClick(View w){
            loopIndicator = true;
            ibtnLoop.setVisibility(View.GONE);
            ibtnStop.setVisibility(View.VISIBLE);

        }
    public void onButtonStopClick(View w){
        loopIndicator = false;
        mpGuitar.stop();
        ibtnLoop.setVisibility(View.VISIBLE);
        ibtnStop.setVisibility(View.GONE);
        }
}
